# Recipe

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. 
The app will automatically reload if you change any of the source files.

## Code scaffolding
Run `ng generate component component-name` or `ng g c path/component` to generate a new component. 

You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

### Starting Recipe
To use style and scripts, put it on angular.json file.

  "styles": [
    "node_modules/bootstrap/dist/css/bootstrap.min.css",
    "src/styles.css"
  ],
  "scripts": [
    "node_modules/bootstrap/dist/js/bootstrap.min.js"
  ]

### create a component inside a specifique path
`ng g c path/component`

### Model

component.model has have the same name of the component in the singular

creating a model - short cut

  /* export class Ingredient {
    public name: string;
    public amount: number;
    constructor(name: string, amount: number) {
      this.name = name;
      this.amount = amount;
    }
  } */

  // equals - is the same below

  export class Ingredient {
    constructor(public name: string, public amount: number) { }
  }

### Debugging
Augury Angular 

### pass property to component from another
Custom property from parent child component use decorator
`@Input() property: Type;`

Pass property whith another name
`<app-recipe-item *ngFor="let recipe of recipes" [propertyName]="recipe"></app-recipe-item>`

Recived the property 
`@Input('propertyName') property: Type;`

without constructor or ngOnInit

### pass property out of a component whith de decorator

`@Output('propertyName') property: Type;`

## SEÇÃO 6 - AULA 81 - SHOPPING LIST
adding local reference to shop-edit on html 

`# nameInput`
`# amountInput`   

Challange: 
cat this #input values as ref on click button, selecting them with at view child 
to create a new ingredient and add it to your ingredients array

add a click listener on the add button passing a local ref or values as arguments
`(click)="onAddtem(arguments)"`

using view child approach add the method above on ShopEditComponent called onAddItem(args)
  
    onAddItem(){

    }

add as properties to ShopEditComponent as
`@ViewChild('nameInput') nameInputRef: ElementRef;`
`@ViewChild('amountInput') amountInputRef: ElementRef;`

remember to import the needed classes
inside the ViewChild pass the local reference

emit a new event where I pass the date to parent component ShopListComponent which manges ingredients array

still in the ShopEditComponent add a new property that has the new ingredient added
passing an object that hold name as string and amount as number being just a type definition
`ingredientAdded = new EventEmitter<{name: string, amount: number}>()`

we can replace
`{name: string, amount: number}` this for or already existing Ingredient model that has the same properties.
`ingredientAdded = new EventEmitter<Ingredients>()`
make sure to import the Ingredient folder

inside inAddItem method we created a new ingredient passing those attributes above
`const ingName = this.nameInputRef.nativeElement.value;`
`const ingAmount = this.amountInputRef.nativeElement.value`
`const newIngredient = new Ingredient(ingName,ingAmount)`

after inside the same method we can set it as our nemIngredient data
`this.ingredientAdded.emit(newIngredient);`

in front of ingredientAdded we can put the decorator
`@Output() ingredientAdded ...`

on the ShopListComponent we can listener our event 
`ingredientAdd="onIngredientAdded()"`

and create a method to handle with new ingredient 
in ShopListComponent that will receive a ingredient with type Ingredient
and inside this method we push into ingredients array the new one

  onIngredientAdded(ingredient: Ingredient){
    this.ingredients.push(ingredient);
  }

## SEÇÃO 7 - AULA 82 - DIRECTIVES

#### Attribute vs Structural

Structural directive change the structure od the dom around this element and this affect overall view container besides to do the same thing that attribute directives

Attribute directive never distroy elements from the dom, it's only change element properties

Scructural start with *
ex: `*ngIf`, `*ngFor`

## SEÇÃO 7 - AULA 83 - ngFor and ngIf

`<li *ngFor="let number of numbers">{{number}}</li>`

We con't use both elements in the same element *ngFor and *ngIf


## SEÇÃO 7 - AULA 84 - ngClass and ngStyle - Conditionals

Conditional to show a class just if the condition is true
`[ngClass]="{class: odd % 2 !== 0}"`

Conditional to show an stile just if condition is true
`[ngStyle]="{backgroundColor: odd % 2 !== 0 ? 'yellow' : 'transparent'}"`

## SEÇÃO 7 - AULA 85 - create attribute directive

create a bassic directive not recommended
to create a directive with ng use
`ng g directive directive_name`

The code alredy comes

  import { Directive } from '@angular/core';
  @Directive({
    selector: '[appBasicHighlight]'
  })
  export class BasicHighlightDirective {
    constructor() { }
  }

all @directives need a selector
`selector: '[appBasicHighlight]'`

when use brackets [] in a selector means that I can use it on html element without brackets [] 

Then we inject a ref type `elementRef` in the constructor as arguments we need When an instance of this class is created 
To transform this elRef as class attribute we need to add `private`
`constructor(private ref: ElementRef){}`

we make the class implements OnInit

to use this ref, inside onInit we put:
`this.elRef.nativeElement.style.backgroundColor = 'green';`

put the new directive in the NgModules
using in a html element the directive
`<p appBasicHighlight>using directive made</p>` 

## SEÇÃO 7 - AULA 86 - create attribute directive

create a better directive called rendered2, 
it's more recommended

creating a new directive
`ng g d directive/better-highlight`

as arg in the constructor using Renderer2
`constructor(private renderer: Renderer2)`

we need to inject the element that use the better directive
`constructor(private elRef: ElementRef, private renderer: Renderer2)`

use it in ngOnInit

  ngOnInit(){
    this.renderer.setStyle(this.elRef.nativeElement,'color', 'blue');
  }

this is a better approach to use directives cause it change the dom directily but use a method, angular isnt limited to running in the broser but also works with service workers and these are env where you not have access to the dom.
If an service using it and the directive try to access the dom we can get an error.
It's a good practice use render for dom access and use the method that render provide to access the dom


## SEÇÃO 7 - AULA 88 - HostListener

#### lietener the host events

Using better a directive beyond change style
in this lesson we change the dom based in event

add a new decorator '@' called HostListener
to listener an mouseover event passint an string the event follow to function_name

  @HostListener('mouseenter') mouseover(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'color', 'blue');
  }

now add the oposite of the mouseover that is mouseleave
  @HostListener('mouseleave') mouseleave(eventData: Event) {
    this.renderer.setStyle(this.elRef.nativeElement, 'color', 'blue');
  }

## SEÇÃO 7 - AULA 89 - HostBinding

#### changing the style from other way

without use the renderer we add the decorator @HostBinding passing a property as type string and  in HostBinding we pass a string to which property we want to bind
`@HostBinding('style.color') color: string;`

that exemple will be used in betterHighlight, so comment the way that the HostListener is change the dom and put the code bellow
`this.color = 'red';`

much more clean the code using HostBinding

to avoid errors put initial value from HostBinding property

## SEÇÃO 7 - AULA 90 - Custom property binding

#### add 2 properties to make it dinamic

`@Input() defaultColor: string = "gray";`
`@Input() highlightColor: string = 'brown';`

now we pass a new property that has a default value instead pass as string color

` @HostBinding("style.color") color: string = this.defaultColor;`
and hostListener action
`this.color = this.highlightColor;`
`this.color = this.defaultColor;`

now we can binding the default color in the html element and the highlightColor too
  
  <p appBetterHighlight
    [defaultColor]="'blue'"
    [highlightColor]="'pink'">
    style me with better directive using Renderer2
  </p>

initialize the default value in ngOnInit(){
  `this.color = this.defaultColor;`
}

using the directive name as argument in @Input property and give it a value.
`@Input('appBetterHighlight') highlightColor: string = "brown";`

in the html element we doesn't need to use the hightlightColor property cause it's ref the directive, but we have to enclose the main directive into square brackets

  <p [appBetterHighlight]="'pink'"
    [defaultColor]="'blue'">
    style me with better directive using Renderer2
  </p>

the same can be used instead highlight property but default property
`@Input('appBetterHighlight') defaultColor: string = "brown";`

to simplify we can remove the square brackets and remove the single quotation marks but careful to make sure that is a property binding and not a real element attribute



