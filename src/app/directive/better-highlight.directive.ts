import { Directive, Renderer2, OnInit,
  ElementRef, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: "[appBetterHighlight]"
})
export class BetterHighlightDirective implements OnInit {
  @Input("appBetterHighlight") defaultColor: string = "gray";
  @Input() highlightColor: string = "brown";
  @HostBinding("style.color") color: string;

  constructor(private elRef: ElementRef, private renderer: Renderer2) {}

  ngOnInit() {
    this.color = this.defaultColor;
    //this.renderer.setStyle(this.elRef.nativeElement,'color', 'blue');
  }

  @HostListener("mouseenter")
  mouseover(eventData: Event) {
    //this.renderer.setStyle(this.elRef.nativeElement, 'color', 'blue');
    this.color = this.highlightColor;
  }

  @HostListener("mouseleave")
  mouseleave(eventData: Event) {
    //this.renderer.setStyle(this.elRef.nativeElement, 'color', 'black');
    this.color = this.defaultColor;
  }
}
