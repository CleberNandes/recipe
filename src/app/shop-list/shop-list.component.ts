import { Component, OnInit } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';

@Component({
  selector: "app-shop-list",
  templateUrl: "./shop-list.component.html",
  styleUrls: ["./shop-list.component.scss"]
})
export class ShopListComponent implements OnInit {
  ingredients: Ingredient[] = [
    new Ingredient("Egg", 2),
    new Ingredient("Apples", 5),
    new Ingredient("Tomatos", 10)
  ];
  constructor() {}

  ngOnInit() {}

  onIngredientAdded(ingredient: Ingredient){
    this.ingredients.push(ingredient);
  }
}
